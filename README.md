# Prosumer Policy

This project aims to model the optimum dispatch behaviour of households with PV and battery systems under different policy instrument mixes. Household electricity consumers with photovoltaics and battery systems are referred to as prosumers since they both produce and consume electricity. This model uses the Gurobi optimizer (plans on adding more soon, contact us if you want to have more interfaces) to determine the optimal household charging behaviour under different policies such as real-time electricity pricing schemes, time varying remunerations schemes and fixed network charges.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This model depends on the packages Numpy and Pandas for data wrangling and [Gurobi](https://www.gurobi.com/documentation/8.0/quickstart_windows/py_python_interface) python package for optimization. Gurobi offers a free license for Academic use.  


### Sample Usage

The interface point between the package and the user is the Model Class. An instance of this class can be created as follows:

```
w=Model()
```
Each model instance has a set of system attributes, policy attributes and optimization parameters.
##### System Attributes
System attributes include, among others, PV and battery system size. Other parameters, such as battery efficiencies etc.. can be changed in the parametrs.yaml file. 

Battery and PV size can be set as follows:

```
w.PV.size=5 #sets PV size to 5 kW
w.Battery.size=5 #sets Battery size to 5 kWh
```
Other parameters can be updated either from the default YAML file or a custom YAML file as follows:

```
w.PV.update_parameters() #updates from default YAML file

w.Battery.update_parameters('customFile.yaml') #updates from custom YAML File
```
##### Policy Attributes  
For each instance of Model there exists a specific regulatory regime made up of:
* RTP: Real Time Electricity Pricing
* VFIT: Variable Feed in Remuneration
* FixedNetworkCharges: Fixed or Volumetric Network charges

Parameters can be defined in YAML file and could also be changed as follows:
```
w.Policy.isRTP=False
```
or through a custom (or default) yaml parameter file
```
w.Policy.update_parameters('customFile.yaml') 
``` 

##### Optimization Parameters
Optimization parameters such as foresight duration and starting day of year can be edited as follows

```
w.timeDuration=24 #sets foresight to 24h
w.day=45 #sets the day 45 of the year
 ```
 ##### Optimization and Results Extracting
With the aforementioned parameters the Model can be optimized:
```
print(w.opt) #returns the dispatch as a DataFrame 
print(w.revenue) #returns the revenue 
``` 
Additional parameters can be computed such as **MAI**, **IRR**, etc.. The **MAI** stands for **M**arket **A**lignment **I**ndicator which measures the performance of a certain instrument mixes in comparison to an ideal case  

## Technical notes on internal validity of the modeling

The technical modeling of the PV-battery system is rather coarse,
the PV generation is an external time series, the battery is characterized
only with charge, discharge and self-discharge parameters. Charge-dependent
efficiencies or inverter interactions are neglected, and battery aging
is only accounted for ex-post after a certain amount of cycles are
reached. All of these factors are hence not part of the optimization.
On the other hand, this would greatly enlarge the optimization problem
and its solving time, and make it a non-linear problem. The impact
of higher technical accuracy modeling could be evaluated in follow-up
studies. As all the cases share the same technical resolution, and
$MAI$ is evaluated relatively and not on absolute levels, we
consider this to be a justifiable complexity reduction approach.

The time variability of the solar input data is underestimated,
as the study considers only the mean average of PV generation in Germany,
neglecting local variations induced by local variability by clouds
etc. Aggregated mean values are chosen, however, as one has to consider
the correlation to wholesale market prices of the same year with the solar
input data. As the generation profile of PV already has a significant
impact on spot market prices in Germany, it is
important to consider generation data that matches with spot market
price time series -- as it means that on average wholesale prices (and
hence RTP) will be lower in sunny hours, and these will need to be
the same hours when the solar generation of the PV system is high.

The optimization interval of the PV-battery system dispatch algorithm
of 'only' 24 hours should be rather unproblematic, as the assumed energy
to power (E/P) ratio of the systems is 2, meaning that the storage
can be completely (dis)charged in 2 hours, making any long-term optimization
dependencies negligible.
 
## Contributing

Please contact us via email to martinklein@posteo.de.


## Authors

* **Ahmad Ziade** - *Initial work* 
* **Martin Klein** - *Initial work* 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Marc Deissenroth, Kristina Nienhaus, Laurens de Vries
